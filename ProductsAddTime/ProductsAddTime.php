<?php

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

use Illuminate\Database\Capsule\Manager as Capsule;

function ProductsAddTime_config()
{
    global $version;
    return [
        "name" => "批量补时长",
        "description" => "ProductsAddTime - 适用于WHMCS的批量补时长插件",
        "version" => $version,
        "author" => "ZeroTime Team",
    ];
}

function ProductsAddTime_output($vars)
{
    include_once(__DIR__ . '/templates/header.php');
    include_once(__DIR__ . '/templates/index.php');
    include_once(__DIR__ . '/templates/footer.php');
}
