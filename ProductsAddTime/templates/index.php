<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$products = Capsule::table("tblproducts")->get();


if (isset($_REQUEST['product']) && isset($_REQUEST['day'])) {
    $now = date("Y-m-d");
    $today = strtotime($now);

    Capsule::beginTransaction();
    $services = Capsule::table("tblhosting")->whereIn('packageid', $_REQUEST['product'])->whereIn('domainstatus', ['Active', 'Suspended'])->where('regdate', "<", $now)->where('nextduedate', "!=", "0000-00-00")->lockForUpdate()->get();
    foreach ($services as $service) {
        $regDate = strtotime($service->regdate);

        $addDate = $today - $regDate;
        if ($addDate > $_REQUEST['day'] * 86400) {
            $addDate = $_REQUEST['day'] * 86400;
        }

        $expireDate = date("Y-m-d", strtotime($service->nextduedate) + $addDate);

        Capsule::table("tblhosting")->where('id', $service->id)->update(['nextduedate' => $expireDate, 'nextinvoicedate' => $expireDate]);
    }
    Capsule::commit();

    echo '<div class="alert alert-success">操作成功!</div>';
}
?>

<table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
    <tbody>
        <tr>
            <td class="fieldlabel">产品</td>
            <td class="fieldarea">
                <select id="products" name="product[]" multiple="multiple">
                    <?php foreach ($products as $product) { ?>
                        <option value="<?php echo $product->id; ?>"><?php echo $product->id; ?>|<?php echo $product->name; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="fieldlabel">时间</td>
            <td class="fieldarea"><input type="number" min="1" name="day" class="form-control input-inline input-400" required>天</td>
        </tr>
    </tbody>
</table>

<button type="submit" class="btn btn-success"><i class="md md-assignment-turned-in"></i>提交</button>

<script type="text/javascript">
    $(function() {
        $("#products").fSelect();
    });
</script>